# Comunicador Social #
O projeto Comunicador Social consiste em oferecer um sistema de comunicação entre usuários 
e deverá conter um cadastro de usuários (nome, apelido e senha), um chat de conversação individual 
(sala de conversa entre duas pessoas) ou um chat em grupo no qual terá um nome, um administrador 
e será privado. Para entrar no programa o visitante deverá se cadastrar sistema informando os dados 
do usuário ou efetuar login com apelido e senha ou. Sempre que um usuário abrir um chat o histórico 
das conversas (sala em grupo ou sala individual) será exibido.


# Etapas de Produção #
 - Jean Kunert - Product Owner
 - Matheus Farias - Reponsavél pela camada do view 
   1. Login 
   2. Junte-se a Nós
   3. Painel de Conversas
      3.1 Contato - Conversa
        3.1.1 Adiconar
        3.1.2 Remover
      3.2 Grupos
        3.2.1 Adicionar  
        3.2.2 Remover 
        3.2.3 Sair
      3.3 Conversa
        3.3.1 Lista conversa Aberta
        3.3.2 Enviar mensagem para Dialogo aberto
   4. Solicitaçoes
      4.1 Aceitar    
      4.2 Rejeitar
      4.3 Listar Pendentes

 - Jarbas Junior - Reponsavél pela camada do controller e Persistencia no banco de dados
   1.Controlador Usuario
	   1.1. Cadastro do Usuario 
	      -> Deve cadastrar o Usuario e verificar se o e-mail já existe
	   1.2. Login do Sistema 
	      -> Realiza o login do sistema, e grava o Usuario validado no Usuario Global
	      -> Valida o e-mail e senha: Email não existe, Senha incorreta
	      -> Login realizado com sucesso.
	   1.3. Atualizar Perfil   
   3. Controlador de Grupo
      3.1 Criar Grupo ( + Nome e Administrador-Criador )
         Para criar o grupo é necessario ter o nome do novo grupo e o administrador 
         que é quem solicita 
      3.2 Adicionar Integrante
         Ao Adicionar o integrante é necessario saber se o Usuario solicitante é um dos administradores do grupo e se o nivel administrativo dele permite essa ação
      3.3 Adicionar Administrador
      	 Recebe um Administrador (Pessoa + NivelAdministrativo) e adiciona na lista de administradores 
      3.4 Atualizar Grupo
         Atualiza o nome do Grupo
      3.5 Remover Integrante 
         Ao Remover o integrante é necessario saber se o Usuario solicitante é um dos administradores do grupo e se o nivel administrativo dele permite essa ação
      3.6 Remover Administrador 
         Ao Remover o integrante é necessario saber se o Usuario solicitante é um dos administradores do grupo e se o nivel administrativo dele permite essa ação
      3.7 Remove Grupo
         Ao Remover verificar se o Usuario solicitante é um dos administradores de grupo e se o nivel Administrativo dele permite essa ação
    4. Controlador Chat
       4.1 Criar Chat
          Para criar o chat é preciso que um Usuario Solicite Amizade com outro deste modo um chat é adicionado representado uma conversa na Janela do Sistema
       4.2 Buscar Chat
          deve buscar chat por Id
       4.3 Remover Chat
          Remover por Id
    5. Controlador Conversa
       5.1 Adicionar Dialogo       
       5.2 Buscar Dialogo
       5.3 Remover Dialogo
    6. Controlador Solicitaçoes 
       6.1 Adicionar Solicitação
       6.1 Buscar Solicitação
       6.1 Aceitar Solicitação
       6.1 Rejeitar Solicitação
       6.1 Rejeitar por periodo Solicitação





          


                 





