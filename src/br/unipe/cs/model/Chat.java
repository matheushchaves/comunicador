package br.unipe.cs.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ForeignKey;

@Entity
@SequenceGenerator(name = "chat_id", sequenceName = "seq_chat", allocationSize = 1)
public class Chat extends AbstractEntity {
	@Id
	private long id;

	@Column(nullable = false)
	private String nome;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date dataCriacao;

	@Temporal(TemporalType.TIME)
	@Column(nullable = false)
	private Date horaCriacao;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_chat", inverseName = "fk_conversa_dialogo")
	private List<Dialogo> conversa;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_chat", inverseName = "fk_integrante_pessoa")
	private List<Pessoa> integrantes;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_chat", inverseName = "fk_admin_pessoa")
	private List<Administrador> administradores;

	public List<Administrador> getAdministradores() {
		return administradores;
	}

	public void setAdministradores(List<Administrador> administradores) {
		this.administradores = administradores;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getHoraCriacao() {
		return horaCriacao;
	}

	public void setHoraCriacao(Date horaCriacao) {
		this.horaCriacao = horaCriacao;
	}

	public List<Dialogo> getConversa() {
		return conversa;
	}

	public void setConversa(List<Dialogo> conversa) {
		this.conversa = conversa;
	}

	public List<Pessoa> getIntegrantes() {
		return integrantes;
	}

	public void setIntegrantes(List<Pessoa> integrantes) {
		this.integrantes = integrantes;
	}

}
