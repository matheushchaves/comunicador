package br.unipe.cs.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Table;

@Entity
@SequenceGenerator(name = "pessoa_id", sequenceName = "seq_pessoa", allocationSize = 1)
public class Pessoa extends AbstractEntity {
	@Id
	@Column(name = "pessoa_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pessoa_id")
	private long id;
	private String nome;

	private String email;
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;

	@Override
	public long getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public void setId(long id) {
		this.id = id;

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return "1 - "+this.getId()+" - "+this.getNome();
//	}
//

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
