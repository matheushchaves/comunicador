package br.unipe.cs.model.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.unipe.cs.model.Pessoa;
import br.unipe.cs.model.dao.PessoaDAO;

public class PessoaService {

	private EntityManager manager;

	public PessoaService(EntityManager manager) {
		this.manager = manager;
	}

	public void salvar(Pessoa pessoa) {
		PessoaDAO pessoaDAO = new PessoaDAO(manager);

		pessoaDAO.salvar(pessoa);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public void atualizar(Pessoa pessoa) {
		PessoaDAO pessoaDAO = new PessoaDAO(manager);

		pessoaDAO.atualizar(pessoa);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public void remover(Pessoa pessoa) {
		PessoaDAO pessoaDAO = new PessoaDAO(manager);

		pessoaDAO.remover(pessoa);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public List<Pessoa> listar() {
		PessoaDAO pessoaDAO = new PessoaDAO(manager);
		return pessoaDAO.listar();

	}

	public Pessoa buscarPorNome(String nome) {
		if (nome == null)
			throw new RuntimeException(
					"public Pessoa buscarPorNome(String nome) - valor null - n�o instanciado ");
		else {
			String jpql = "select a from Pessoa a where a.nome = :nomePessoa";
			Query query = manager.createQuery(jpql, Pessoa.class);
			query.setParameter("nomePessoa", nome);
			return (Pessoa) query.getSingleResult();
		}

	}
	public Pessoa buscarPorEmail(String email) {
		if (email == null)
			throw new RuntimeException(
					"public Pessoa buscarPorEmail(String email) - valor null - n�o instanciado ");
		else {
			String jpql = "select a from Pessoa a where a.email = :emailPessoa";
			Query query = manager.createQuery(jpql, Pessoa.class);
			query.setParameter("emailPessoa", email);
			return (Pessoa) query.getSingleResult();
		}

	}

}
