package br.unipe.cs.model.service;

import java.util.List;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Usuario;
import br.unipe.cs.model.dao.UsuarioDAO;

public class UsuarioService {

	private EntityManager manager;

	public UsuarioService(EntityManager manager) {
		this.manager = manager;
	}

	public void salvar(Usuario usuario) {
		UsuarioDAO usuarioDAO = new UsuarioDAO(manager);

		usuarioDAO.salvar(usuario);
		manager.getTransaction().begin();
		manager.getTransaction().commit();
	}

	public void atualizar(Usuario usuario) {
		UsuarioDAO usuarioDAO = new UsuarioDAO(manager);

		usuarioDAO.atualizar(usuario);
		manager.getTransaction().begin();
		manager.getTransaction().commit();
	}

	public void remover(Usuario usuario) {
		UsuarioDAO usuarioDAO = new UsuarioDAO(manager);

		usuarioDAO.remover(usuario);
		manager.getTransaction().begin();
		manager.getTransaction().commit();
	}

	public Usuario buscarPorNome(String nome){
		UsuarioDAO usuarioDAO = new UsuarioDAO(manager);
		return null;
	}
	
	public List<Usuario> listar() {
		UsuarioDAO usuarioDAO = new UsuarioDAO(manager);
		return usuarioDAO.listar();
	}
	
	public Usuario buscarPorEmail(String email){
		UsuarioDAO usuarioDAO = new UsuarioDAO(manager);
		return usuarioDAO.buscarPorEmail(email);
			
	}
	
	
	

}
