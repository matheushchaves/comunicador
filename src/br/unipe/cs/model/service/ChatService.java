package br.unipe.cs.model.service;

import java.util.List;


import javax.persistence.EntityManager;

import br.unipe.cs.model.Chat;
import br.unipe.cs.model.dao.ChatDAO;

public class ChatService {
	private EntityManager manager;

	public ChatService(EntityManager manager) {
		this.manager = manager;
	}

	
	public void salvar(Chat chat) {
		ChatDAO chatDAO = new ChatDAO(manager);

		chatDAO.salvar(chat);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}
	public void atualizar(Chat chat) {
		ChatDAO chatDAO = new ChatDAO(manager);

		chatDAO.atualizar(chat);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}
	public void remover(Chat chat) {
		ChatDAO chatDAO = new ChatDAO(manager);

		chatDAO.remover(chat);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}


	public List<Chat> listar() {
		ChatDAO chatoDAO = new ChatDAO(manager);
		return chatoDAO.listar();

	}

}
