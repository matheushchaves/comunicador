package br.unipe.cs.model.service;

import java.util.List;


import javax.persistence.EntityManager;

import br.unipe.cs.model.Dialogo;
import br.unipe.cs.model.dao.DialogoDAO;


public class DialogoService {

	private EntityManager manager;

	public DialogoService(EntityManager manager) {
		this.manager = manager;
	}

	public void salvar(Dialogo dialogo) {
		DialogoDAO dialogoDAO = new DialogoDAO(manager);

		dialogoDAO.salvar(dialogo);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public void atualizar(Dialogo dialogo) {
		DialogoDAO dialogoDAO = new DialogoDAO(manager);

		dialogoDAO.atualizar(dialogo);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}
	public void remover(Dialogo dialogo) {
		DialogoDAO dialogoDAO = new DialogoDAO(manager);

		dialogoDAO.remover(dialogo);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public List<Dialogo> listar() {
		DialogoDAO dialogoDAO = new DialogoDAO(manager);
		return dialogoDAO.listar();

	}


}
