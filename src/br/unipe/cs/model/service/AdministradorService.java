package br.unipe.cs.model.service;

import java.util.List;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Administrador;
import br.unipe.cs.model.Chat;
import br.unipe.cs.model.dao.AdministradorDAO;
import br.unipe.cs.model.dao.ChatDAO;

public class AdministradorService {
	private EntityManager manager;

	public AdministradorService(EntityManager manager) {
		this.manager = manager;
	}

	public void salvar(Administrador admin) {
		AdministradorDAO adminDAO = new AdministradorDAO(manager);

		adminDAO.salvar(admin);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public void atualizar(Administrador admin) {
		AdministradorDAO adminDAO = new AdministradorDAO(manager);

		adminDAO.atualizar(admin);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public void remover(Administrador admin) {
		AdministradorDAO adminDAO = new AdministradorDAO(manager);

		adminDAO.remover(admin);

		manager.getTransaction().begin();
		manager.getTransaction().commit();

	}

	public List<Administrador> listar() {
		AdministradorDAO adminDAO = new AdministradorDAO(manager);
		return adminDAO.listar();

	}

}
