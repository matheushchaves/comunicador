package br.unipe.cs.model;

public enum StatusLogin {
	
	USUARIO_CRIADO_COM_SUCESSO, SENHA_INCORRETA, EMAIL_INCORRETO, EMAIL_JA_EXISTE

}
