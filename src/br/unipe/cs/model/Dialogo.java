package br.unipe.cs.model;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



import java.util.Date;

import javax.persistence.Entity;

import org.hibernate.annotations.ForeignKey;
@Entity
@SequenceGenerator(name="dialogo_id",sequenceName="seq_dialog",allocationSize=1)
public class Dialogo extends AbstractEntity {
	
	@Id
	private long id;
	@Temporal(TemporalType.DATE)
	private Date dataMensagem;
	@Temporal(TemporalType.TIME)
	private Date horaMensagem;
	
	@OneToOne
	@ForeignKey(name="fk_usuario",inverseName="fk_mensagem_usuario")
	private Pessoa pessoaMensagem;
	
	private String mensagem;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDataMensagem() {
		return dataMensagem;
	}

	public void setDataMensagem(Date dataMensagem) {
		this.dataMensagem = dataMensagem;
	}

	public Date getHoraMensagem() {
		return horaMensagem;
	}

	public void setHoraMensagem(Date horaMensagem) {
		this.horaMensagem = horaMensagem;
	}


	public Pessoa getPessoaMensagem() {
		return pessoaMensagem;
	}

	public void setPessoaMensagem(Pessoa pessoaMensagem) {
		this.pessoaMensagem = pessoaMensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
