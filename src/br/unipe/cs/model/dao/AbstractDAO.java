package br.unipe.cs.model.dao;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.unipe.cs.model.AbstractEntity;

public abstract class AbstractDAO<T extends AbstractEntity> {

	protected EntityManager manager;

	public AbstractDAO(EntityManager manager) {
		this.manager = manager;
	}
	
	public void salvar(T entity){
		manager.persist(entity);
	}
	
	public void atualizar(T entity){
		manager.merge(entity);
	}
	
	public void remover(T entity){
		entity = manager.find(entityClass(), entity.getId());
		manager.remove(entity);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> listar(){
		Query query = manager.createQuery("select c from " + entityClass().getSimpleName() + " c");
		return query.getResultList();
	}	

	public T buscarPorId(long id){
		return manager.find(entityClass(), id);
	}
	public T buscarPorNome(String nome){
		return manager.find(entityClass(), nome);
	}
	
	public abstract Class<T> entityClass();
}
	

