package br.unipe.cs.model.dao;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Chat;

public class ChatDAO extends AbstractDAO<Chat> {
	public ChatDAO(EntityManager manager) {
		super(manager);
	}

	@Override
	public Class<Chat> entityClass() {
		return Chat.class;
	}

}
