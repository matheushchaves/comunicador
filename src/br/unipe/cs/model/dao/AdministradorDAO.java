package br.unipe.cs.model.dao;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Administrador;

public class AdministradorDAO  extends AbstractDAO<Administrador> {
	public AdministradorDAO(EntityManager manager) {
		super(manager);
	}

	@Override
	public Class<Administrador> entityClass() {
		return Administrador.class;
	}

}
