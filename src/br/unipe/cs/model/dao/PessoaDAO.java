package br.unipe.cs.model.dao;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Pessoa;

public class PessoaDAO extends AbstractDAO<Pessoa>{

	
	public PessoaDAO(EntityManager manager) {
		super(manager);
	}

	
	@Override
	public Class<Pessoa> entityClass() {
		// TODO Auto-generated method stub
		return Pessoa.class;
	}

}
