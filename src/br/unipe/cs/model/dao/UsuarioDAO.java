package br.unipe.cs.model.dao;

import javax.persistence.EntityManager;

import javax.persistence.Query;

import br.unipe.cs.model.Usuario;

public class UsuarioDAO extends AbstractDAO<Usuario> {

	public UsuarioDAO(EntityManager manager) {
		super(manager);
	}

	@Override
	public Class<Usuario> entityClass() {
		return Usuario.class;
	}

	public Usuario buscarPorEmail(String email) {
		Usuario usuarioRetorno = null ;
		String consulta = "select c from " + entityClass().getSimpleName()
				+ " c " ;// where c.email like '"+email+"' ";

		System.out.println(consulta);
		Query query = manager.createQuery(consulta);
		
		if (!query.getResultList().isEmpty()) 
		    return usuarioRetorno = (Usuario) query.getResultList().get(0); 
		return usuarioRetorno;
	}

}
