package br.unipe.cs.model.dao;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Dialogo;

public class DialogoDAO extends AbstractDAO<Dialogo>{
	public DialogoDAO(EntityManager manager) {
		super(manager);
	}

	@Override
	public Class<Dialogo> entityClass() {
		return Dialogo.class;
	}

}
