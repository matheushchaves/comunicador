package br.unipe.cs.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Table;

@Entity
@SequenceGenerator(name = "admin_id", sequenceName = "seq_admin", allocationSize = 1)
public class Administrador extends AbstractEntity {

	@Id
	@Column(name = "admin_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "admin_id")
	private long id;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_admin", inverseName = "fk_pessoa_admin")
	private Pessoa administrador;
	private NivelAdministrativo nivel;

	public NivelAdministrativo getNivel() {
		return nivel;
	}

	public void setNivel(NivelAdministrativo nivel) {
		this.nivel = nivel;
	}

	@Override
	public long getId() {
		return this.id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

}
