package br.unipe.cs.model;

import javax.persistence.*;

import java.util.Date;

@Entity
public class Usuario extends Pessoa {

	@Column(nullable = false)
	private String senha;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date dataCadastro;

	private String mensagemPerfil;

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public String getMensagemPerfil() {
		return mensagemPerfil;
	}

	public void setMensagemPerfil(String mensagemPerfil) {
		this.mensagemPerfil = mensagemPerfil;
	}
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return "2 - "+this.getId()+" - "+this.getNome();
//	}

}
