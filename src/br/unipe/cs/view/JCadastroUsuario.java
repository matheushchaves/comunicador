package br.unipe.cs.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import java.awt.Label;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JCadastroUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtInformeOSeu;
	private JFormattedTextField textField_dataNascimento;
	private JPasswordField passwordField_senha;
	private JTextField textField_Email;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JCadastroUsuario frame = new JCadastroUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JCadastroUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 330);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 424, 53);
		panel.setForeground(SystemColor.textHighlight);
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		contentPane.add(panel);
		
		JLabel lblSejaBemvindo = new JLabel("Seja bem-vindo !!!");
		lblSejaBemvindo.setHorizontalAlignment(SwingConstants.CENTER);
		lblSejaBemvindo.setFont(new Font("Calibri", Font.ITALIC, 30));
		panel.add(lblSejaBemvindo);
		
		JLabel lblNewLabel = new JLabel("Preencha as seguintes informa\u00E7\u00F5es para entrar na nossa rede:");
		lblNewLabel.setBounds(15, 69, 307, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nome Completo");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(14, 94, 127, 21);
		contentPane.add(lblNewLabel_1);
		
		txtInformeOSeu = new JTextField();
		txtInformeOSeu.setToolTipText("Informe o seu nome completo");
		txtInformeOSeu.setBounds(15, 126, 406, 20);
		contentPane.add(txtInformeOSeu);
		txtInformeOSeu.setColumns(10);
		
		JLabel lblDataNascimento = new JLabel("Data de Nascimento");
		lblDataNascimento.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDataNascimento.setBounds(15, 157, 127, 21);
		contentPane.add(lblDataNascimento);
		
		textField_dataNascimento = new JFormattedTextField();
		textField_dataNascimento.setToolTipText("Informe a data do seu Nascimento");
		textField_dataNascimento.setBounds(145, 157, 86, 20);
		contentPane.add(textField_dataNascimento);
		textField_dataNascimento.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenha.setBounds(241, 157, 44, 21);
		contentPane.add(lblSenha);
		
		passwordField_senha = new JPasswordField();
		passwordField_senha.setToolTipText("Informe a senha desejada");
		passwordField_senha.setBounds(301, 157, 120, 21);
		contentPane.add(passwordField_senha);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail.setBounds(16, 185, 50, 21);
		contentPane.add(lblEmail);
		
		textField_Email = new JTextField();
		textField_Email.setToolTipText("Informe o e-mail no qual ser\u00E1 vinculado seu cadastro");
		textField_Email.setColumns(10);
		textField_Email.setBounds(17, 217, 406, 20);
		contentPane.add(textField_Email);
		
		JButton btnProsseguir = new JButton("Prosseguir");
		btnProsseguir.setToolTipText("Clique aqui para prosseguir com o procediemento");
		btnProsseguir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnProsseguir.setBounds(185, 260, 110, 23);
		contentPane.add(btnProsseguir);
		
		JButton btnAbandonar = new JButton("Abandonar");
		btnAbandonar.setToolTipText("Clique aqui para abandonar o procedimento");
		btnAbandonar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAbandonar.setBounds(314, 260, 110, 23);
		contentPane.add(btnAbandonar);
		
	}
}
