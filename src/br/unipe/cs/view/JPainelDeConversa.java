package br.unipe.cs.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.CardLayout;

import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.border.TitledBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.CompoundBorder;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

public class JPainelDeConversa extends JFrame {
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private Container conteinerPrincipal;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JPainelDeConversa frame = new JPainelDeConversa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void montarPropriedadesJanela() {
		conteinerPrincipal = getContentPane();
		conteinerPrincipal.setLocation(-165, -393);
		setTitle("Painel de Conversas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 655);
		conteinerPrincipal.setLayout(null);

	}

	private void montarPainelContatos() {
		// --- Painel Contatos - INI --- //
		JPanel panel = new JPanel();
		panel.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192,
				192), 2), new LineBorder(new Color(255, 255, 255), 2)));
		panel.setBounds(11, 11, 152, 480);
		conteinerPrincipal.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Contatos");
		lblNewLabel.setBounds(10, 11, 93, 14);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		panel.add(lblNewLabel);

		JPanel panel_4 = new JPanel();
		panel_4.setBounds(10, 416, 132, 53);
		panel.add(panel_4);
		panel_4.setLayout(null);
		panel_4.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192,
				192), 2), new LineBorder(new Color(255, 255, 255), 2)));

		JButton btnRc = new JButton("R.C");
		btnRc.setBounds(71, 11, 51, 36);
		panel_4.add(btnRc);

		JButton btnIc = new JButton("I.C");
		btnIc.setBounds(9, 11, 50, 36);
		panel_4.add(btnIc);

		panel.add(panel_4);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 132, 369);
		panel.add(scrollPane);
		
				JList list = new JList();
				scrollPane.setViewportView(list);
				list.setBackground(UIManager.getColor("ToolTip.background"));
				list.setFont(new Font("Tahoma", Font.PLAIN, 14));
				list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				list.setModel(new AbstractListModel() {
					String[] values = new String[] { "Matheus Farias", "Jarbas Junior",
							"Eric Neder", "Flavio Boiola", "Jean Reolon" };

					public int getSize() {
						return values.length;
					}

					public Object getElementAt(int index) {
						return values[index];
					}
				});

		// --- Painel Contatos - FIM --- //

	}

	private void montarPainelGrupo() {
		// --- Painel Grupo - INI --- //
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192,
				192), 2), new LineBorder(new Color(255, 255, 255), 2)));
		panel_1.setBounds(623, 11, 152, 480);
		conteinerPrincipal.add(panel_1);

		JLabel lblGrupos = new JLabel("Grupos");
		lblGrupos.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblGrupos.setBounds(10, 11, 93, 20);
		panel_1.add(lblGrupos);

		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBorder(new CompoundBorder(new LineBorder(new Color(192, 192,
				192), 2), new LineBorder(new Color(255, 255, 255), 2)));
		panel_5.setBounds(10, 416, 132, 53);
		panel_1.add(panel_5);

		JButton btnRg = new JButton("R.G");
		btnRg.setBounds(69, 11, 51, 36);
		panel_5.add(btnRg);

		JButton btnIg = new JButton("I.G");
		btnIg.setBounds(9, 11, 50, 36);
		panel_5.add(btnIg);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 132, 369);
		panel_1.add(scrollPane);
				
						JList list_1 = new JList();
						scrollPane.setViewportView(list_1);
						list_1.setModel(new AbstractListModel() {
							String[] values = new String[] {"Inteligence", "Unip\u00EA", "Familia"};
							public int getSize() {
								return values.length;
							}
							public Object getElementAt(int index) {
								return values[index];
							}
						});
						list_1.setBackground(UIManager.getColor("ToolTip.background"));
						list_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						list_1.setFont(new Font("Tahoma", Font.PLAIN, 14));


		// --- Painel Grupo - FIM --- //

	}

	private void montarPainelConversa() {
		// --- Painel Conversa - INI --- //
		JPanel painelConversa = new JPanel();
		painelConversa.setLayout(null);
		painelConversa.setBorder(new CompoundBorder(new LineBorder(new Color(192,
				192, 192), 2), new LineBorder(new Color(255, 255, 255), 2)));
		painelConversa.setBounds(169, 11, 446, 381);
		conteinerPrincipal.add(painelConversa);

		JLabel lblConversa = new JLabel("Conversa");
		lblConversa.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblConversa.setBounds(10, 11, 93, 14);
		painelConversa.add(lblConversa);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 428, 334);
		painelConversa.add(scrollPane);
		
				table = new JTable();
				table.setBackground(SystemColor.inactiveCaption);
				scrollPane.setViewportView(table);
				table.setBorder(new LineBorder(new Color(0, 0, 0), 0));
				table.setShowHorizontalLines(false);
				table.setShowVerticalLines(false);
				table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				table.setModel(new DefaultTableModel(new Object[][] {
						{ "08/04/2015", "19:50:01", "Matheus",
								"Ei macho, pq o source tree nao ta pegando?" },
						{ "08/04/2015", "19:52:05", "Jean", null }, }, new String[] {
						"Data", "Hora", "Pessoa", "Mensagem" }) {
					Class[] columnTypes = new Class[] { String.class, String.class,
							String.class, String.class };

					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}
				});
	}

	private void montarPainelMensagem() {
		// --- Painel Mensagem - INI --- //

		JPanel painelMensagem = new JPanel();
		painelMensagem.setLayout(null);
		painelMensagem
				.setBorder(new CompoundBorder(new LineBorder(new Color(192,
						192, 192), 2), new LineBorder(new Color(255, 255, 255),
						2)));
		painelMensagem.setBounds(168, 393, 446, 98);
		conteinerPrincipal.add(painelMensagem);

		JLabel lblMensagem = new JLabel("Mensagem");
		lblMensagem.setBounds(10, 11, 93, 14);
		painelMensagem.add(lblMensagem);
		lblMensagem.setFont(new Font("Tahoma", Font.BOLD, 16));

		JButton btnEnviar = new JButton("#Send");
		btnEnviar.setBounds(389, 36, 47, 51);
		painelMensagem.add(btnEnviar);

		JTextPane textPane_Mensagem = new JTextPane();
		textPane_Mensagem.setBackground(UIManager.getColor("ToolTip.background"));
		textPane_Mensagem.setBounds(10, 36, 369, 51);
		painelMensagem.add(textPane_Mensagem);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new CompoundBorder(new LineBorder(new Color(192,
								192, 192), 2), new LineBorder(new Color(255, 255, 255),
								2)));
		panel.setBounds(11, 502, 764, 98);
		getContentPane().add(panel);
		
		JLabel lblSolicitaes = new JLabel("Solicita\u00E7\u00F5es");
		lblSolicitaes.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSolicitaes.setBounds(10, 11, 106, 14);
		panel.add(lblSolicitaes);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 31, 744, 56);
		panel.add(scrollPane);
		
		table_1 = new JTable();
		table_1.setBackground(Color.YELLOW);
		table_1.setForeground(Color.BLACK);
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
				{"Aguardando...", "Solicita\u00E7\u00E3o de Amizade", "Mayara Vanessa"},
				{"Aguardando...", "Requisi\u00E7\u00E3o de Amizade", "Adriano Nascimento"},
			},
			new String[] {
				"Status", "Tipo", "Solicitante"
			}
		));
		table_1.getColumnModel().getColumn(0).setPreferredWidth(96);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(140);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(303);
		table_1.setShowVerticalLines(false);
		table_1.setShowHorizontalLines(false);
		table_1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_1.setBorder(new LineBorder(new Color(0, 0, 0), 0));
		scrollPane.setViewportView(table_1);
		table.getColumnModel().getColumn(3).setPreferredWidth(354);

	}

	/**
	 * Create the frame.
	 */
	public JPainelDeConversa() {
		montarPropriedadesJanela();
		montarPainelContatos();
		montarPainelGrupo();
		montarPainelConversa();
		montarPainelMensagem();
	}
}
