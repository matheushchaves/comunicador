package br.unipe.cs.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Checkbox;
import java.awt.SystemColor;

import javax.swing.UIManager;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import java.awt.Toolkit;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class JLogin extends JFrame {

	private JPanel contentPane;
	private JTextField textField_Email;
	private JPasswordField passwordField_senha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JLogin frame = new JLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JLogin() {
		
		setTitle("Comunicador Social");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 423, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("E-mail");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(20, 79, 71, 23);
		contentPane.add(lblNewLabel);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblSenha.setBounds(20, 113, 71, 23);
		contentPane.add(lblSenha);
		
		textField_Email = new JTextField();
		textField_Email.setToolTipText("Informe o e-mail para realizar o login");
		textField_Email.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_Email.setBounds(91, 79, 227, 20);
		contentPane.add(textField_Email);
		textField_Email.setColumns(10);
		
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setVerticalAlignment(SwingConstants.BOTTOM);
		btnEntrar.setToolTipText("Clique aqui para ter acesso ao Comunicador Social");
		btnEntrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnEntrar.setBounds(163, 147, 111, 23);
		contentPane.add(btnEntrar);
		
		JButton btnRetornar = new JButton("Retornar");
		btnRetornar.setToolTipText("Clique aqui para abandonar o Comunicador Social");
		btnRetornar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRetornar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnRetornar.setBounds(284, 147, 113, 23);
		contentPane.add(btnRetornar);
		
		Checkbox checkbox = new Checkbox("Lembrar Senha ?");
		checkbox.setBounds(223, 113, 111, 22);
		contentPane.add(checkbox);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 5, true));
		panel.setForeground(UIManager.getColor("Table.selectionBackground"));
		panel.setBounds(0, 0, 407, 57);
		contentPane.add(panel);
		
		JLabel lblComunicadorSocial = new JLabel("Comunicador Social");
		lblComunicadorSocial.setHorizontalAlignment(SwingConstants.CENTER);
		lblComunicadorSocial.setFont(new Font("Calibri", Font.ITALIC, 30));
		panel.add(lblComunicadorSocial);
		
		JButton btnNewButton = new JButton("Junte-se a n\u00F3s...");
		btnNewButton.setToolTipText("Clique aqui para realizar o seu cadastro e conversar com seus amigos e colegas...");
		btnNewButton.setFont(new Font("Cambria Math", Font.PLAIN, 14));
		btnNewButton.setBounds(20, 147, 133, 23);
		contentPane.add(btnNewButton);
		
		passwordField_senha = new JPasswordField();
		passwordField_senha.setToolTipText("Informe a senha para realizar o login");
		passwordField_senha.setBounds(91, 113, 111, 20);
		contentPane.add(passwordField_senha);
		
	}
}
