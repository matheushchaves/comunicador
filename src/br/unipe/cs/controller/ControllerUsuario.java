package br.unipe.cs.controller;

import javax.persistence.EntityManager;

import br.unipe.cs.model.StatusLogin;
import br.unipe.cs.model.Usuario;
import br.unipe.cs.model.service.UsuarioService;

public class ControllerUsuario {

	private UsuarioService usuarioService;
	private ControllerPessoa controllerPessoa;

	public ControllerUsuario(EntityManager manager) {
		usuarioService = new UsuarioService(manager);
		controllerPessoa = new ControllerPessoa(manager);
	}

	public StatusLogin criarConta(Usuario u) {

		Usuario emailUsuario = usuarioService.buscarPorEmail(u.getEmail());
		if (emailUsuario == null) {
			usuarioService.salvar(u);
			return StatusLogin.USUARIO_CRIADO_COM_SUCESSO;
		} else
			return StatusLogin.EMAIL_JA_EXISTE;
	}

	public void editarPerfil(Usuario usuario) {
		usuarioService.atualizar(usuario);
	}

	public void removerConta(Usuario usuario) {
		usuarioService.remover(usuario);
	}

	public Usuario buscarPorEmail(String email) {
		return usuarioService.buscarPorEmail(email);
	}
	
	

}
