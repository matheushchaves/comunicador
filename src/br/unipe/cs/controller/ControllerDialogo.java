package br.unipe.cs.controller;

import java.util.List;


import javax.persistence.EntityManager;

import br.unipe.cs.model.Chat;
import br.unipe.cs.model.Pessoa;

public class ControllerDialogo {
	private ControllerPessoa controllerPessoa;
	private ControllerChat controllerChat;

	public ControllerDialogo(EntityManager manager) {
		controllerPessoa = new ControllerPessoa(manager);
		controllerChat = new ControllerChat(manager);
	}

	public boolean adicionarAmigo(Pessoa pessoa) {

		return true;
	}

	public boolean removerAmigo(Pessoa pessoa) {

		return true;
	}

	public boolean apagarConversa(Chat chat) {

		return true;
	}

	public boolean buscarConversa(Chat chat) {

		return true;
	}

	public List<Pessoa> listarAmigos() {
		return null;
	}

}
