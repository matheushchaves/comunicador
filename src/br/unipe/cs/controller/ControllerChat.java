package br.unipe.cs.controller;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Chat;
import br.unipe.cs.model.service.ChatService;

public class ControllerChat {

	private ChatService chatService;
	
	public ControllerChat(EntityManager manager) {
		chatService = new ChatService(manager);
	}

	public boolean iniciar(Chat chat) {

		chatService.salvar(chat);

		return true;
	}

	public boolean buscar(Chat chat) {

		chatService.atualizar(chat);

		return true;
	}

	public boolean apagar(Chat chat) {

		chatService.remover(chat);

		return true;
	}

	
	
}
