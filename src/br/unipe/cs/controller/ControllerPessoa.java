package br.unipe.cs.controller;

import javax.persistence.EntityManager;

import br.unipe.cs.model.Pessoa;
import br.unipe.cs.model.service.PessoaService;

public class ControllerPessoa {
	private PessoaService pessoaService;

	public ControllerPessoa(EntityManager manager) {
		pessoaService = new PessoaService(manager);
	}

	public boolean cadastrar(Pessoa pessoa) {

		pessoaService.salvar(pessoa);

		return true;
	}

	public boolean remover(Pessoa pessoa) {

		pessoaService.remover(pessoa);

		return true;
	}

	public boolean existeSolicitacao(Pessoa pessoa) {

		if (pessoaService.buscarPorEmail(pessoa.getEmail()) == null)
			return false;
		return true;

	}

}
